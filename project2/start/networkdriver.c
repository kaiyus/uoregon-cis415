#include "networkdriver.h"
#include "BoundedBuffer.h"
#include "freepacketdescriptorstore__full.h"
#include "networkdevice__full.h"
#include "diagnostics.h"
#include <stdio.h>
#include <pthread.h>

#define UNUSED(x) (void)(x)

pthread_t send_thread, recv_thread;
NetworkDevice *mydevice;
FreePacketDescriptorStore *mystore;
BoundedBuffer *sendBB, *poolBB, *getBB[MAX_PID+1];

void *recv_f(void *);
void *send_f(void *);

void init_network_driver(NetworkDevice *nd, void *mem_start, unsigned long mem_length, FreePacketDescriptorStore **fpds_ptr){
	int i, size;
	PacketDescriptor *this;
	mydevice = nd;
	mystore = FreePacketDescriptorStore_create(mem_start, mem_length);
	*fpds_ptr = mystore;
	size = ((mystore -> size(mystore))/3);
	sendBB = BoundedBuffer_create(size);
	poolBB = BoundedBuffer_create(size);
	for (i = 0; i < size; ++i){
		mystore -> blockingGet(*fpds_ptr, &this);
		poolBB -> blockingWrite(poolBB, this);
	}
	size = size/(MAX_PID + 1);
	if ((size/(MAX_PID + 1)) < 2)
		size = 2;
	for (i = 0; i <= MAX_PID; ++i)
		getBB[i] = BoundedBuffer_create(size);
	pthread_create(&send_thread, NULL, send_f, NULL);
	pthread_create(&recv_thread, NULL, recv_f, NULL);
}


void blocking_send_packet(PacketDescriptor *pd){
	sendBB -> blockingWrite(sendBB, pd);
}

int nonblocking_send_packet(PacketDescriptor *pd){
	return sendBB -> nonblockingWrite(sendBB, pd);
}

void blocking_get_packet(PacketDescriptor **pd, PID pid){
	getBB[pid] -> blockingRead(getBB[pid], (void**)pd);
}

int nonblocking_get_packet(PacketDescriptor **pd, PID pid){
	return getBB[pid] -> nonblockingRead(getBB[pid], (void**)pd);
}

int non_blocking_get_packet(PacketDescriptor **pd){
	if (!(poolBB -> nonblockingRead(poolBB, (void**)pd)))
		if (!(mystore -> nonblockingGet(mystore, pd)))
			return 0;
	return 1;
}

int non_blocking_put_packet(PacketDescriptor *pd){
	if (!(poolBB -> nonblockingWrite(poolBB, pd)))
		if (!(mystore -> nonblockingPut(mystore, pd)))
			return 0;
	return 1;
}

void *recv_f(void * arg){
	PacketDescriptor *current, *fail;
	PID p;
	UNUSED(arg);
	mystore -> blockingGet(mystore, &current);
	initPD(current);
	mydevice -> registerPD(mydevice, current);
	while(1){
		mydevice -> awaitIncomingPacket(mydevice);
		fail = current;
		if(non_blocking_get_packet(&current)){
			initPD(current);
			mydevice -> registerPD(mydevice, current);
			p = getPID(fail);
			if (getBB[p] -> nonblockingWrite(getBB[p], fail) == 0)
				non_blocking_put_packet(fail);
		}else{
			current = fail;
			initPD(current);
			mydevice -> registerPD(mydevice, current);
		}
	}
	return NULL;
}

void *send_f(void * arg){
	PacketDescriptor *temp;
	int i, j;
	UNUSED(arg);
	while(1){
		sendBB -> blockingRead(sendBB, (void**)&temp);
		j = 0;
		for (i = 0; i < 10; ++i){
			j = mydevice -> sendPacket(mydevice, temp);
			if (j == 1)
				break;
		}
		switch(j){
			case 1:
				DIAGNOSTICS("success to send a packet after %d times.\n", i);
				break;
			case 0:
				DIAGNOSTICS("fail to send a packet after %d times.\n", i);
				break;
			default:
				break;
		}
		non_blocking_put_packet(temp);
	}
	return NULL;
}