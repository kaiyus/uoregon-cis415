#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <sys/time.h>

/*workqueue def*/

typedef struct workqueue WorkQueue;
typedef struct worknode WorkNode;

struct worknode{
	char* filename;
	WorkNode* next;
};

struct workqueue{
	int size;
	WorkNode* head;
	WorkNode* tail;
};

WorkNode* worknodecreate(char* name){
	WorkNode *wn = (WorkNode *)malloc(sizeof(WorkNode));
	if (wn == NULL){
		return NULL;
	}
	wn -> filename = strdup(name);
	wn -> next = NULL;
	return wn;
}

WorkQueue* workqueuecreate(){
	WorkQueue *wq = (WorkQueue *)malloc(sizeof(WorkQueue));
	if (wq == NULL){
		return NULL;
	}
	wq -> head = NULL;
	wq -> tail = NULL;
	wq -> size = 0;
	return wq;
}

void enqueue(WorkQueue* wq, WorkNode* wn){
	if (wq -> size == 0){
		wq -> head = wn;
		wq -> tail = wn;
	}else{
		wq -> tail -> next = wn;
		wq -> tail = wn;
		wq -> tail -> next = NULL;
	}
	wq -> size = wq -> size + 1;
}

WorkNode* dequeue(WorkQueue* wq){
	if(wq -> size == 0){
		return NULL;
	}
	WorkNode* wn;
	wn = wq -> head;
	wq -> head = wq -> head -> next;
	wn -> next = NULL;
	wq -> size = wq -> size - 1;
	return wn;
}

void worknodedestory(WorkNode* wn){
	wn -> next = NULL;
	free(wn -> filename);
	free(wn);
}

void workqueuedestory(WorkQueue* wq){
	while((wq -> size) != 0){
		worknodedestory(dequeue(wq));
	}
	free(wq);
}

void workqueueadd(WorkQueue* wq, char* name){
	enqueue(wq, worknodecreate(name));
}

int workqueuecheck(WorkQueue* wq, char* name){
	WorkNode* wn = wq -> head;
	while(wn != NULL){
		if (!strcmp(wn->filename, name)){
			return 1;
		}
		wn = wn -> next;
	}
	return 0;
}








/*hashmap def*/

typedef struct hashmap HashMap;
typedef struct hashnode HashNode;

#define TABLE_SIZE 134217728L 

struct hashnode{
	char* key;
	WorkQueue* value;
	HashNode* next;
};

struct hashmap{
	long count;
	long size;
	HashNode** nodearray;
};

long hashcode(char *keyname, long size) { 
    // int sum = 0; 
    // int shift = 16; 
    // int i; 
    // for (i = 0; keyname[i] != '\0'; i++) 
    //     sum = shift * sum + (int)keyname[i]; 
    // return sum % size;
    long ans = 0L;
    char *sp;
    for (sp = keyname; *sp != '\0'; sp++){
        ans = ((7L * ans) + *sp) % size;
    }
	return ans;
}

HashNode* hashmapsearch(HashMap *hm, char *keyname) { 
    HashNode *result; 
    long hv = hashcode(keyname, hm->size); 
    for (result = hm->nodearray[hv]; result != NULL; result = result->next) { 
        if (strcmp(keyname, result->key) == 0) 
            return result; 
    } 
    return NULL; 
}

void changevaluebykey(HashMap *hm, char *keyname, WorkQueue* newvalue) { 
    HashNode *hn = hashmapsearch(hm, keyname);
    hn->value = newvalue;
}

HashNode* hashnodecreate(char* kn, WorkQueue* wq){
	HashNode* hn = (HashNode *)malloc(sizeof(HashNode));
	if (hn == NULL){
		return NULL;
	}
	hn -> key = strdup(kn);
	hn -> value = wq;
	hn -> next = NULL;
	return hn;
}

void hashnodedestory(HashNode* hn){
	if (hn != NULL){
		hn -> next = NULL;
		workqueuedestory(hn -> value);
		free(hn -> key);
		free(hn);
	}
}

void hashmapdestory(HashMap* hm){
	long i;
	for (i = 0; i < hm -> size; ++i){
		hashnodedestory((hm->nodearray)[i]);
	}
	free(hm);
}

HashMap* hashmapcreate(){
	HashMap* hm = (HashMap *)malloc(sizeof(HashMap));
	if (hm == NULL){
		return NULL;
	}
	HashNode** array = (HashNode **)malloc(TABLE_SIZE * sizeof(HashNode *));
	if (array == NULL){
		free(hm);
		return NULL;
	}
	hm -> nodearray = array;
	hm -> count = 0;
	hm -> size = TABLE_SIZE;
	return hm;
}

void hashmapinsert(HashMap* hm, HashNode* hn){
	long hv = hashcode(hn->key, hm->size);
	// printf("%li    %li\n", hm->size, hv);
	// printf("%d\n", ((hm->nodearray)[hv])==NULL);
	hn -> next = hm -> nodearray[hv];
	hm -> nodearray[hv] = hn;
	// printf("%d\n", ((hm->nodearray)[hv])==NULL);
	hm -> count ++;
}

void hashmapadd(HashMap* hm, char* keyname, WorkQueue* v){
	HashNode* hn = hashnodecreate(keyname, v);
	hashmapinsert(hm, hn);
}

WorkQueue* getvaluebykey(HashMap* hm, char* keyname){
	return (hashmapsearch(hm, keyname) -> value);
}

WorkQueue* workqueue;
HashMap* hashmap;

// int main(int argc, char *argv[])
// {
	
// 	workqueue = workqueuecreate();
// 	workqueueadd(workqueue, "aaa");
// 	workqueueadd(workqueue, "bbb");
// 	workqueueadd(workqueue, "ccc");
// 	workqueueadd(workqueue, "ddd");
// 	workqueueadd(workqueue, "eee");
// 	printf("%d\n", workqueuecheck(workqueue, "aaa"));
// 	printf("%d\n", workqueuecheck(workqueue, "fff"));
// 	printf("%d\n", workqueue->size);
// 	printf("%s\n", workqueue->head->filename);
// 	printf("%s\n", workqueue->tail->filename);
// 	dequeue(workqueue);
// 	printf("%d\n", workqueue->size);
// 	printf("%s\n", workqueue->head->filename);
// 	printf("%s\n", workqueue->tail->filename);
// 	// workqueuedestory(workqueue);
	


// 	// HashNode* temp;
// 	// WorkQueue* wq1 = workqueuecreate();
// 	// WorkQueue* wq2 = workqueuecreate();
// 	// WorkQueue* wq3 = workqueuecreate();
// 	// workqueueadd(wq1, "A1");
// 	// workqueueadd(wq1, "A2");
// 	// workqueueadd(wq1, "A3");
// 	// workqueueadd(wq2, "B1");
// 	// workqueueadd(wq2, "B2");
// 	// workqueueadd(wq2, "B3");
// 	// workqueueadd(wq2, "B4");
// 	// workqueueadd(wq3, "C1");
// 	// dequeue(wq2);
// 	// hashmap = hashmapcreate();
// 	// HashNode* hn1 = hashnodecreate("AA", wq1);
// 	// HashNode* hn2 = hashnodecreate("BB", wq2);
// 	// HashNode* hn3 = hashnodecreate("CC", wq3);
// 	// hashmapinsert(hashmap, hn1);
// 	// hashmapinsert(hashmap, hn2);
// 	// hashmapinsert(hashmap, hn3);
// 	// temp = hashmapsearch(hashmap, "AA");
// 	// printf("%s\n", temp->value->head->filename);
// 	// printf("%s\n", temp->value->tail->filename);
// 	// temp = hashmapsearch(hashmap, "BB");
// 	// printf("%s\n", temp->value->head->filename);
// 	// printf("%s\n", temp->value->tail->filename);
// 	// temp = hashmapsearch(hashmap, "CC");
// 	// printf("%s\n", temp->value->head->filename);
// 	// printf("%s\n", temp->value->tail->filename);
// 	// printf("%s\n", getvaluebykey(hashmap, "CC")->tail->filename);
// 	// hashmapdestory(hashmap);
// 	return 0;
// }