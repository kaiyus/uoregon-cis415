#include "p1fxns.h"
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define BUFLEN 1024

struct comnode {
	struct comnode* next;
	char* name;
	char** com;
	pid_t id;
};

int wordcont(char* str){
	int i = 0;
	char buffer[BUFLEN];
	int cont = 0;
	while(i>=0){
		i = p1getword(str, i, buffer);
		cont++;
	}
	return cont++;
}

char** wordpass(char* com){
	int n = wordcont(com);
	char** array = (char **)malloc(sizeof(char *)*(n+1));
	if (array == NULL){
		return NULL;
	}
	array[n] = NULL;
	int i = 0;
	char buffer[BUFLEN];
	int index = 0;
	while(i>=0){
		i = p1getword(com, i, buffer);
		array[index] = p1strdup(buffer);
		index++;
	}
	return array;
}

struct comnode* nodecreate(char* commond){
	struct comnode* node = (struct comnode*)malloc(sizeof(struct comnode));
	if (node == NULL){
		return NULL;
	}
	node -> next = NULL;
	node -> com = wordpass(commond);
	node -> name = p1strdup(node -> com[0]);
	node -> id = 0;
	return node;
}

struct comnode* build(int fd){
	struct comnode *start = NULL, *currect=NULL;
	char buffer[BUFLEN];
	while(p1getline(fd, buffer, BUFLEN)>0){
		int l = p1strlen(buffer);
		if (buffer[l-1]=='\n')
		{
			buffer[l-1]='\0';
		}
		if (start == NULL){
			start = nodecreate(buffer);
			currect = start;
		}else{
			currect->next = nodecreate(buffer);
			currect = currect->next;
		}
	}
	return start;
}

void destory(struct comnode* node){
	if (node!=NULL){
		destory(node->next);
		free(node->name);
		char** it = node->com;
		while(*it!=0){
			free(*it);
			it++;
		}
		free(node->com);
		free(node);
	}
}

void processhandle(struct comnode* startnode){
	struct comnode* currect = startnode;
	while(currect != NULL){
		currect->id = fork();
		if (currect->id == 0){
			execvp(currect->name, (currect->com)+1);
			destory(startnode);
			exit(0);
		}else{
			currect = currect->next;
		}
	}
	currect = startnode;
	while(currect != NULL){
		wait(&(currect->id));
		currect=currect->next;
	}
}

int main(int argc, char* argv[])
{
	if (argc > 3){
		return 0;
	}
	if (argc==1){
		if (getenv("USPS_QUANTUM_MSEC") == NULL){
			return 0;
		}
	}
	int f = 0;
	int val = 0;
	if (argc>1){
		if (p1strneq(argv[1], "--quantum=", 10))
		{
			val = p1atoi(argv[1]+10);
			f = 1;
		}
	}else if (getenv("USPS_QUANTUM_MSEC") != NULL){
		val = p1atoi(getenv("USPS_QUANTUM_MSEC"));
	}
	if (val == 0)
	{
		return 0;
	}
	int fd;
	if (argc == 2){
		if (f){
			fd = 0;
		}else{
			fd = open(argv[1], O_RDONLY);
		}
	}else if (argc == 3){
		fd = open(argv[2], O_RDONLY);
	}else{
		fd = 0;
	}
	struct comnode* startnode = build(fd);
	close(fd);
	processhandle(startnode);
	destory(startnode);
	return 0;
}