#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "date.h"

int main(int argc, char const *argv[])
{
	char* a = "14/01/2000";
	char* b = "14/02/2000";
	char* c = "23/02/2000";
	char* d = "14/02/1994";
	const Date *d1, *d2, *d3, *d4;
	d1 = Date_create(a);
	d2 = Date_create(b);
	d3 = Date_create(c);
	d4 = Date_create(d);
	printf("%d\n", d1->compare(d1, d4));
	return 0;
}