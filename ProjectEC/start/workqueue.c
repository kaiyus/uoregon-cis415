#include "workqueue.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct worknode{
	char* filename;
	WorkNode* next;
};

struct workqueue{
	int size;
	WorkNode* head;
	WorkNode* tail;
};

WorkNode* worknodecreate(char* name){
	WorkNode *wn = (WorkNode *)malloc(sizeof(WorkNode));
	wn -> filename = strdup(name);
	wn -> next = NULL;
	return wq;
}

WorkQueue* workqueuecreate(){
	WorkQueue *wq = (WorkQueue *)malloc(sizeof(WorkQueue));
	if (wq == NULL){
		return NULL;
	}
	wq -> head = NULL;
	wq -> tail = NULL;
	wq -> size = 0;
	return wq;
}

void enqueue(WorkQueue* wq, WorkNode* wn){
	if (wq -> size == 0){
		wq -> head = wn;
		wq -> tail - wn;
	}else{
		wq -> tail -> next = wn;
		wq -> tail = wn;
		wq -> tail -> next = NULL;
	}
	wq -> size = wq -> size + 1;
}

WorkNode* dequeue(WorkQueue* wq){
	if(wq -> size == 0){
		return NULL;
	}
	WorkNode* wn;
	wn = wq -> head;
	wq -> head = wq -> head -> next;
	wn -> next = NULL;
	wq -> size = wq -> size - 1;
	return wn;
}

void worknodedestory(WorkNode* wn){
	wn -> next = NULL;
	free(wn -> filename);
	free(wn);
}

void workqueuedestory(WorkQueue* wq){
	while((wq -> size) != 0){
		worknodedestory(dequeue(wq));
	}
	free(wq);
}

void workqueueadd(WorkQueue* wq, char* name){
	enqueue(wq, worknodecreate(name));
}