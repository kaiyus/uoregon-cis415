#include "uqueue.h"
#include "hashmap.h"
#include <stdlib.h>
#include <stdio.h>

UQueue* workqueue;
HashMap* hashmap;

int main(int argc, char  *argv[]){
	workqueue = uq_create();
	// printf("%li\n", uq_size(workqueue));
	uq_add(workqueue, "abc");
	// uq_add(workqueue, (void*)123);
	// printf("%li\n", uq_size(workqueue));
	// uq_clear(workqueue, NULL);
	// printf("%li\n", uq_size(workqueue));
	void * ele;
	// uq_remove(workqueue, &ele);
	// printf("%s\n", ele);
	// uq_remove(workqueue, &ele);
	// printf("%d\n", ele);
	hashmap = hm_create(0, 0.0);
	// printf("%d\n", hm_isEmpty(hashmap));
	char* keyname = "abc";
	void* previous;
	hm_put(hashmap, keyname, workqueue, &previous);
	printf("%d\n", previous==NULL);
	// printf("%d\n", hm_isEmpty(hashmap));
	hm_get(hashmap, keyname, &previous);
	printf("%d\n", previous==NULL);
	uq_peek(previous, &ele);
	printf("%s\n", ele);
	return 0;
}