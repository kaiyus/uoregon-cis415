#ifndef _WORKQUEUE_H_INCLUDED_
#define _WORKQUEUE_H_INCLUDED_

typedef struct workqueue WorkQueue;
typedef struct worknode WorkNode;

WorkNode* worknodecreate(char* name);
WorkQueue* workqueuecreate();
void enqueue(WorkQueue* wq, WorkNode* wn);
WorkNode* dequeue(WorkQueue* wq);
void worknodedestory(WorkNode* wn);
void workqueuedestory(WorkQueue* wq);
void workqueueadd(WorkQueue* wq, char* name);

#endif
