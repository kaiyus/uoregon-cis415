#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#define PATHSIZE 1024

char* pathlist[PATHSIZE];
int plindex = 0;
int startplace = 1;

char* removewhitespace(char* source){
	int i, j, k;
	char* result = (char *)malloc(sizeof(strlen(source)));
	k = strlen(source);
	j = 0;
	for (i = 0; i < k; ++i){
		if (source[i] != ' '){
			if (source[i] != '\t'){
				result[j] = source[i];
				++j;
			}
		}
	}
	return result;
}

void legalfailenamecheck(char* filename){
	char* res = strrchr(filename, '.')+1;
	if (!((*res=='c')||(*res=='y')||(*res=='l'))){
		exit(1);
	}
}

char* getfilename(char* f){
	int i, l;
	char* result = (char *)malloc(sizeof(strlen(f)));
	l = strlen(f);
	for (i = 0; i < l; ++i){
		if (f[i] == '.'){
			return result;
		}
		result[i] = f[i];
	}
	return NULL;
}

char* ofile(char* filename){
	char* result = (char *)malloc(sizeof(strlen(filename)));
	result = getfilename(filename);
	strcat(result, ".o");
	return result;
}

int includecheck(char* str){
	char* inc = "#include";
	int i;
	for (i = 0; i < 8; ++i){
		if (str[i] != inc[i]){
			return 0;
		}
	}
	return 1;
}

int flagcheck(char* str){
	char* inc = "-I";
	int i;
	for (i = 0; i < 2; ++i){
		if (str[i] != inc[i]){
			return 0;
		}
	}
	return 1;
}

void separatercpath(char* cpath){
	char* cp = strdup(cpath);
	char* path;
	path = strtok(cp, ":");
	while(path != NULL){
		size_t l = strlen(path);
		char* p = (char*)malloc(l+1);
		strcpy(p, path);
		if (path[l-1] != '/'){
			p[l] = '/';
		}
		pathlist[plindex] = p;
		plindex++;
		path = strtok (NULL, ":");
	}
}

char* includeget(char* line){
	int i, j, k, l;
	char* result = (char *)malloc(sizeof(strlen(line)));
	k = strlen(line);
	j = l = 0;
	for (i = 0; i < k; ++i){
		if (l){
			if (line[i] == '"'){
				// printf("%s\n", result);
				return result;
			}
			result[j] = line[i];
			j++;
		}
		if (line[i] == '"'){
			l = 1;
		}
	}
	return NULL;
}

void pladd(char* pathname){
	size_t l = strlen(pathname);
	char* p = (char*)malloc(l+1);
	strcpy(p, pathname);
	if (pathname[l-1] != '/'){
		p[l] = '/';
	}
	pathlist[plindex] = p+2;
	plindex++;
}

// int main(int argc, char *argv[]){
// 	pathlist[0] = "./";
// 	plindex++;
// 	if (flagcheck(argv[1])){
// 		pladd(argv[1]);
// 		startplace++;
// 	}
// 	int i;
// 	for (i = 0; i < plindex; ++i){
// 		// printf("The %d is %s\n", i, pathlist[i]);
// 	}
// 	for (i = startplace; i < argc; ++i){
// 		printf("%s\n", argv[i]);
// 	}
// 	// printf("%d\n", includecheck("abcd"));
// 	// printf("%d\n", includecheck("#includeabc"));
// 	// printf("%s\n", removewhitespace("a b c d e"));
// 	// printf("%s\n", getfilename("test1.c"));
// 	// printf("%d\n", legalfailenamecheck("test.out"));
// 	// printf("%d\n", legalfailenamecheck("test.k"));
// 	// printf("%d\n", legalfailenamecheck("test.c"));
// 	// printf("%d\n", legalfailenamecheck("test.y"));
// 	// printf("%d\n", legalfailenamecheck("test.l"));
// 	// printf("%d\n", legalfailenamecheck("test.exe"));
// 	// separatercpath("abc/:def:ghi");
// 	// char* get = includeget("ab\"cd\"ef\"gh\"i");
// 	// printf("%s\n", get);
// 	// char* cpath = getenv("CPATH");
// 	// printf("%d\n", cpath==NULL);
// 	// separatercpath(cpath);
// 	return 0;
// }