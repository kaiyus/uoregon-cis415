#include "test2.c"
#include "test1.c"

int openfile(char* filename){
	int i;
	for (i = 0; i < plindex; ++i){
		char* p = strdup(pathlist[i]);
		strcat(p, filename);
		int fd = open(p, O_RDONLY);
		if (fd >= 0){
			return fd;
		}
	}
	// exit(1);
	return -1;
}

// WorkQueue* includetest(char* filename){
// 	WorkQueue* includethings = workqueuecreate();
// 	int fd = openfile(filename);
// 	char line[300];
// 	char* temp = (char *)malloc(300 * sizeof(char *));
// 	while(mygetline(fd, line, 300)>0){
// 		temp = removewhitespace(line);
// 		if ((includecheck(temp))&&(includeget(temp)!=NULL)){
// 			// printf("%s\n", includeget(temp));
// 			workqueueadd(includethings, includeget(temp));
// 			// printf("%s\n", includethings->tail->filename);
// 		}
// 	}
// 	return includethings;
// }

WorkQueue* process(char* afile, WorkQueue* deps){
	int fd = openfile(afile);
	if (fd == -1){
		printf("Could not open file %s\n", afile);
		return NULL;
	}
	printf("Now is file %s\n", afile);
	char line[300];
	char* temp = (char *)malloc(300 * sizeof(char *));
	while(mygetline(fd, line, 300)>0){
		temp = removewhitespace(line);
		if ((includecheck(temp))&&(includeget(temp)!=NULL)){
			temp = includeget(temp);
			workqueueadd(deps, temp);
			if (hashmapsearch(hashmap, temp)==NULL){
				printf("    Here %s\n", temp);
				WorkQueue* nq = workqueuecreate();
				printf("    Here %s\n", temp);
				hashmapadd(hashmap, temp, nq);
				workqueueadd(workqueue, temp);
			}
			// printf("%s\n", includeget(temp));
			// workqueueadd(includethings, includeget(temp));
			// printf("%s\n", includethings->tail->filename);
		}
	}
	close(fd);
	return deps;
}

int mygetline(int fd, char buf[], int size) {
    int i;
    char c;
    int max = size - 1;
    i = 0;
    for (i = 0; i < max; i++) {
        if (read(fd, &c, 1) == 0)
            break;
        buf[i] = c;
        if (c == '\n') {
            i++;
            break;
        }
    }
    buf[i] = '\0';
    return i;
}

void printdeps(WorkQueue* printed, WorkQueue* toprocess){
	WorkNode* wn = dequeue(toprocess);
	WorkQueue* ll;
	while(wn != NULL){
		ll = getvaluebykey(hashmap, wn->filename);
		WorkNode* temp = ll->head;
		while(temp != NULL){
			char* name = temp->filename;
			if (!workqueuecheck(printed, name)){
				printf("%s ", name);
				workqueueadd(printed, name);
				workqueueadd(toprocess, name);
			}
			temp = temp -> next;
		}
		wn = dequeue(toprocess);
	}
}

int main(int argc, char *argv[]){
	if (argc < 2){
		exit(1);
	}
	workqueue = workqueuecreate();
	hashmap = hashmapcreate();
	pathlist[0] = "./";
	plindex++;
	for (startplace; startplace < argc; startplace++){
		if (flagcheck(argv[startplace])){
			pladd(argv[startplace]);
		}else{
			break;
		}
	}
	char* cpath = getenv("PATH");
	if (cpath != NULL){
		separatercpath(cpath);
	}
	int i;
	WorkQueue* pq;
	for (i = startplace; i < argc; ++i){
		legalfailenamecheck(argv[i]);
		pq = workqueuecreate();
		workqueueadd(pq, argv[i]);
		hashmapadd(hashmap, ofile(argv[i]), pq);
		workqueueadd(workqueue, argv[i]);
		pq = workqueuecreate();
		hashmapadd(hashmap, argv[i], pq);
	}
	WorkNode* wn = workqueue->head;
	WorkQueue* deps;
	// printf("%d\n", );
	while(wn!=NULL){
		// printf("%s\n", wn->filename);
		deps = getvaluebykey(hashmap, wn->filename);
		deps = process(wn->filename, deps);
		changevaluebykey(hashmap, wn->filename, deps);
		wn = dequeue(workqueue);
	}
	for (i = startplace; i < argc; ++i){
		char* obj = ofile(argv[i]);
		WorkQueue* printed = workqueuecreate();
		printf("%s ", obj);
		workqueueadd(printed, obj);
		WorkQueue* toprocess = workqueuecreate();
		workqueueadd(toprocess, obj);
		printdeps(printed, toprocess);
		printf("\n");
	}
}