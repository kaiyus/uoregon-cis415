#include "p1fxns.h"
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <sys/time.h>

#define BUFLEN 1024
#define UNUSED(x) (void)(x)

volatile int run = 0;
volatile int quantum = 0;
int np = 0;

struct comnode {
	struct comnode* next;
	char* name;
	char** com;
	pid_t id;
	int s1;
	int alive;
};

struct comnode* nodearray[0];
struct comnode* currect = NULL;

typedef struct queue Queue;
Queue* queue;
Queue* exitqueue;

typedef struct queue{
	struct comnode *tail;
	struct comnode *head;
	int size;
}Queue;

void destory(struct comnode* node){
	if (node!=NULL){
		node->next = NULL;
		free(node->name);
		char**it=node->com;
		while(*it != 0){
			free(*it);
			it++;
		}
		free(node->com);
		free(node);
	}
}

Queue* queuecreate(){
	Queue *nq = (Queue *)malloc(sizeof(Queue));
	if (nq == NULL){
		return NULL;
	}
	nq -> head = NULL;
	nq -> tail = NULL; 
	nq -> size = 0;
	return nq;

}

void enqueue(Queue *q, struct comnode* newnode){
	if(q->size == 0){
		q -> head = newnode;
		q -> tail = newnode;
	} else {
		q -> tail -> next = newnode;
		q -> tail = newnode;
		q -> tail -> next = NULL;
	}
	q -> size += 1;           
} 

struct comnode* dequeue(Queue *q){
	if(q->size == 0){
		return NULL;
	}
	struct comnode *node;
	node = q -> head;
	q -> head = q -> head -> next;
	node -> next = NULL;
	q -> size = q -> size - 1;
	return node;
}

void display(struct comnode* node){
	pid_t pid = node -> id;
	char displayvalue[BUFLEN];
    char buf[BUFLEN];
    char b[BUFLEN];
    int i, j, l;

    char * path1 = (char *)malloc(sizeof(char *)*BUFLEN);
    path1[0] = '\0';
    buf[0] = '\0';
    p1strcat(path1, "/proc/");
    p1itoa(pid, buf);
    p1strcat(path1, buf);
    p1strcat(path1, "/io");
    int io = open(path1, O_RDONLY);
    if (io == -1){
    	free(path1);
    	return;
    }
    p1getline(io, buf, BUFLEN);
	l = p1strlen(buf);
	if (buf[l-1]=='\n'){
		buf[l-1]='\0';
	}
    p1strcat(displayvalue, buf);
    p1strcat(displayvalue, "  ");
    p1getline(io, buf, BUFLEN);
	l = p1strlen(buf);
	if (buf[l-1]=='\n'){
		buf[l-1]='\0';
	}
    p1strcat(displayvalue, buf);
    p1strcat(displayvalue, "  ");

	close(io);
	free(path1);

    char * path2 = (char *)malloc(sizeof(char *)*BUFLEN);
    path2[0] = '\0';
    buf[0] = '\0';
    p1strcat(path2, "/proc/");
    p1itoa(pid, buf);
    p1strcat(path2, buf);
    p1strcat(path2, "/stat");
    int stat = open(path2, O_RDONLY);
    if (stat == -1){
    	free(path2);
    	return;
    }
    p1getline(stat, buf, BUFLEN);
    i = j = 0;
    while(j>= 0){
    	j = p1getword(buf, j, b);
    	if (i == 0){
    		p1strcat(displayvalue, b);
    		p1strcat(displayvalue, "  ");
    	}else if (i == 2){
    		p1strcat(displayvalue, b);
    		p1strcat(displayvalue, "  ");
    	}else if (i == 9){
    		p1strcat(displayvalue, b);
    		p1strcat(displayvalue, "  ");
    	}else if (i == 14){
    		p1strcat(displayvalue, b);
    		p1strcat(displayvalue, "  ");
    	}else if (i == 15){
    		p1strcat(displayvalue, b);
    		p1strcat(displayvalue, "  ");
    	}else if (i == 23){
    		p1strcat(displayvalue, b);
    		p1strcat(displayvalue, "  ");
    	}else if (i == 24){
    		p1strcat(displayvalue, b);
    		p1strcat(displayvalue, "  ");
    	}
    	i++;
    }
	close(stat);
	free(path2);

    char * path3 = (char *)malloc(sizeof(char *)*BUFLEN);
    path3[0] = '\0';
    buf[0] = '\0';
    p1strcat(path3, "/proc/");
    p1itoa(pid, buf);
    p1strcat(path3, buf);
    p1strcat(path3, "/cmdline");
    int cmdline = open(path3, O_RDONLY);
    if (cmdline == -1){
    	free(path3);
    	return;
    }
    p1getline(cmdline, buf, BUFLEN);
    p1strcat(displayvalue, buf);
	close(cmdline);
	free(path3);

	p1strcat(displayvalue, "\n");
	p1putstr(1, displayvalue);
}

void sigalarm(){
	if(currect != NULL){
		if (currect->alive == 0){
			enqueue(exitqueue, currect);
			currect = NULL;
		}else{
			kill(currect->id, SIGSTOP);
			display(currect);
			enqueue(queue, currect);
			currect = NULL;
		}
	}
	while(currect == NULL){
		struct comnode* node = dequeue(queue);
		if (node != NULL){
			if (node->alive == 0){
				enqueue(exitqueue, node);
			}else{
				currect = node;
				if (currect->s1==0){
					kill(currect->id, SIGUSR1);
					currect->s1 = 1;
				}else{
					kill(currect->id, SIGCONT);
				}
			}
		}else{
			break;
		}
	}
}

void sigchild(){
	int status;
	pid_t pid;
	int i = 0;	
	while ((pid = waitpid(-1, &status, WNOHANG)) > 0) {
		if(WIFEXITED(status) || WIFSIGNALED(status)){
			for (i = 0; i < np; ++i){
				if (nodearray[i]->id == pid){
					nodearray[i]->alive = 0;
				}
			}
		}
	}
}

void sigusr1(int signal){
	UNUSED(signal);
	run =1;
}

void do_nanosleep(int nseconds){
	struct timespec time, time2;
	time.tv_sec = 0;
	time.tv_nsec = nseconds;
	time2.tv_sec =0;
	time2.tv_nsec = 0;
	nanosleep(&time, &time2);
}

void wait_and_execvp(char *cmd,  char* argv[]){
	while(run==0){
		do_nanosleep(1);			
	}
	int status;
	pid_t pid;	
	pid = waitpid(-1, &status, WNOHANG);
	execvp(cmd,argv);
	int i;
	for (i = 0; i < np; ++i){
		if (nodearray[i]->id == pid){
			nodearray[i]->alive=0;
		}
	}
	exit(0);
}

int wordcont(char* str){
	int i = 0;
	char buffer[BUFLEN];
	int cont = 0;
	while(i>=0){
		i = p1getword(str, i, buffer);
		cont++;
	}
	return cont++;
}

char** wordpass(char* com){
	int n = wordcont(com);
	char** array = (char **)malloc(sizeof(char *)*(n+1));
	if (array == NULL){
		return NULL;
	}
	array[n] = NULL;
	int i = 0;
	char buffer[BUFLEN];
	int index = 0;
	while(i>=0){
		i = p1getword(com, i, buffer);
		array[index] = p1strdup(buffer);
		index++;
	}
	return array;
}

struct comnode* ncreate(char* commond){
	struct comnode* node = (struct comnode*)malloc(sizeof(struct comnode));
	if (node == NULL){
		return NULL;
	}
	node -> next = NULL;
	node -> com = wordpass(commond);
	node -> name = p1strdup(node -> com[0]);
	node -> id = 0;
	node -> s1 = 0;
	node -> alive = 1;
	return node;
}

void build(int fd){
	char buffer[BUFLEN];
	while(p1getline(fd, buffer, BUFLEN)>0){
		int l = p1strlen(buffer);
		if (buffer[l-1]=='\n'){
			buffer[l-1]='\0';
		}
		nodearray[np] = ncreate(buffer);
		np++;
	}
}

void timer(void){
	struct itimerval new_value;
	new_value.it_value.tv_sec = quantum/1000;
	new_value.it_value.tv_usec = (quantum*1000) % 1000000;
	new_value.it_interval = new_value.it_value;
	setitimer(ITIMER_REAL, &new_value, 0);
}

void newprocess(){
	signal(SIGUSR1, sigusr1);
	signal(SIGALRM, sigalarm);
	signal(SIGCHLD, sigchild);
	int i = 0;
	while(i<np){
		nodearray[i]->id = fork();
		if (nodearray[i]->id == 0){
			wait_and_execvp(nodearray[i]->name, (nodearray[i]->com)+1);
		}
		i++;
	}
	for (i = 0; i < np; ++i){
		enqueue(queue, nodearray[i]);
	}
	timer();
	while(queue->size != 0){
		pause();
	}
}

int main(int argc, char* argv[])
{
	if (argc > 3){
		return 0;
	}
	if (argc==1){
		if (getenv("USPS_QUANTUM_MSEC") == NULL){
			return 0;
		}
	}
	int f = 0;
	int val = 0;
	if (argc>1){
		if (p1strneq(argv[1], "--quantum=", 10)){
			val = p1atoi(argv[1]+10);
			f = 1;
		}
	}else if (getenv("USPS_QUANTUM_MSEC") != NULL){
		val = p1atoi(getenv("USPS_QUANTUM_MSEC"));
	}
	if (val == 0){
		return 0;
	}
	quantum = val;
	int fd;
	if (argc == 2){
		if (f){
			fd = 0;
		}else{
			fd = open(argv[1], O_RDONLY);
		}
	}else if (argc == 3){
		fd = open(argv[2], O_RDONLY);
	}else{
		fd = 0;
	}
	build(fd);
	close(fd);
	queue = queuecreate();
	exitqueue = queuecreate();
	newprocess();
	while(queue->size != 0){
		destory(dequeue(queue));
	}
	while(exitqueue->size != 0){
		destory(dequeue(exitqueue));
	}
	free(queue);
	free(exitqueue);
	return 0;
}