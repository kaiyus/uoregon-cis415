#include "date.h"
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

typedef struct date_data {
    int day;
    int month;
    int year;
} DateData;

static int date_compare();
static void date_destroy();
static const Date *date_duplicate();

static Date template = {NULL, date_duplicate, date_compare, date_destroy};


static int date_compare(const Date *date1, const Date *date2){
	DateData *d1 = (DateData *)(date1->self);
	DateData *d2 = (DateData *)(date2->self);
	return (d1->year - d2->year)*10000 + (d1->month - d2->month)*100 + (d1->day - d2->day);
}

static void date_destroy(const Date *d){
	DateData *dad = (DateData *)(d->self);
	free(dad);
	free((void *)d);
}

//example of *d: pointer to another Date
static const Date *date_duplicate(const Date *d){
	Date *copy = (Date *)malloc(sizeof(Date));

	if (copy != NULL){
		DateData *copydata = (DateData *)malloc(sizeof(DateData));
		if (copydata != NULL){
			//ItData *itd = (ItData *)(it->self);
			DateData *dd = (DateData *)(d->self);
			copydata -> day = dd -> day;
			copydata -> month = dd -> month;
			copydata -> year = dd -> year;
			*copy = template;
			copy -> self = copydata;
		} else {
			free(copy);
			copy = NULL;
		}
	}
	return copy;
}


const Date *Date_create(char *datestr){
	//e.m.: 14/02/2000
	Date *date = (Date *)malloc(sizeof(Date));

	if (date != NULL){
		DateData *dad = (DateData *)malloc(sizeof(DateData));
		if (dad != NULL){
			int d, m, y;
			if (sscanf(datestr, "%d/%d/%d", &d, &m, &y)!=3){
				free(date);
				date = NULL;
				return date;
			}
			dad -> day = d;
			dad -> month = m;
			dad -> year = y;
			*date = template;
			date -> self = dad;
		} else {
			free(date);
			date = NULL;
		}
	}
	return date;
}