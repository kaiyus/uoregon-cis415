#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <pthread.h>
#include "tshashmap.h"
#include "tsuqueue.h"

/*do these*/
/*mkdir kaiyus*/
/*cp include_crawler.c uqueue.c uqueue.h hashmap.c hashmap.h iterator.c iterator.h tsuqueue.c tsuqueue.h tshashmap.c kaiyus*/
/*cp tshashmap.h tsiterator.c tsiterator.h linkedlist.c linkedlist.h report.txt Makefile kaiyus*/ 
/*tar -zcvf kaiyus-extracredit.tgz $(cat manifest)*/
/*tar -ztvf kaiyus-extracredit.tgz*/

#define PATHSIZE 100

char* pathlist[PATHSIZE];
int plindex = 0;
int startplace = 1;
int threadnumber = 2;
int activethreads = 0;
const TSUQueue* workqueue;
const TSHashMap* hashmap;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

/*helper function to check if file name is end with c, y or l*/
void legalfailenamecheck(char* filename){
	char* res = strrchr(filename, '.') + 1;
	if (!((*res == 'c')||(*res == 'y')||(*res == 'l'))){
		printf("please enter only c, y and l file\n");
		exit(1);
	}
}

/*helper fucntion to get file name before '.'*/
char* getfilename(char* f){
	int i, l;
	l = strlen(f);
	char* result = (char*)malloc(sizeof(l));
	for (i = 0; i < l; ++i){
		if (f[i] == '.')
			return result;
		result[i] = f[i];
	}
	return NULL;
}

/*helper function to build .o file name base in old one*/
char* ofile(char* filename){
	char* result = (char*)malloc(sizeof(strlen(filename)));
	result = getfilename(filename);
	strcat(result, ".o");
	return result;
}

/*helper function to check if a line is a include line and not system call*/
int includecheck(char* str){
	char* inc = "#include";
	int i, r1, r2;
	r1 = 1;
	r2 = 0;
	for (i = 0; i < 8; ++i){
		if (str[i] != inc[i])
			r1 = 0;
	}
	for (i = 0; i < strlen(str); ++i){
		if (str[i] == '"')
			r2 = 1;
	}
	return (r1&&r2);
}

/*helper fucntion to check if the input is sepecific address*/
int flagcheck(char* str){
	char* inc = "-I";
	int i;
	for (i = 0; i < 2; ++i){
		if (str[i] != inc[i])
			return 0;
	}
	return 1;
}

/*helper fucntion to separatert cpath into path list*/
void separatercpath(char* cpath){
	char* path;
	path = strtok(cpath, ":");
	while(path != NULL){
		size_t l = strlen(path);
		char* p = (char*)malloc(l+1);
		strcpy(p, path);
		if (path[l-1] != '/')
			p[l] = '/';
		pathlist[plindex] = p;
		plindex++;
		path = strtok (NULL, ":");
	}
}

/*add new element in path list*/
void pathlineadd(char* pathname){
	size_t l = strlen(pathname);
	char* p = (char*)malloc(l+1);
	strcpy(p, pathname);
	if (pathname[l-1] != '/')
		p[l] = '/';
	pathlist[plindex] = p+2;
	plindex++;
}

/*open a file base on input file name and path address list, if can't open it return -1*/
int openfile(char* filename){
	int i;
	for (i = 0; i < plindex; ++i){
		char* p = strdup(pathlist[i]);
		strcat(p, filename);
		int fd = open(p, O_RDONLY);
		if (fd >= 0)
			return fd;
	}
	return -1;
}

/*helper function to get one line from a file and put it intp buf[] (SP: from project 1 hepers functinos)*/
int mygetline(int fd, char buf[], int size) {
	int i;
	char c;
	int max = size - 1;
	i = 0;
	for(i = 0; i < max; i++){
		if (read(fd, &c, 1) == 0)
			break;
		buf[i] = c;
		if (c == '\n'){
			i++;
			break;
		}
	}
	buf[i] = '\0';
	return i;
}

/*helper function to remve the white space and tab from one line*/
void removespaces(char* source){
	char* i = source;
	char* j = source;
	while(*j != 0){
		*i = *j++;
		if((*i != ' ')&&(*i != '\t'))
			i++;
	}
	*i = 0;
}

/*hepler function to get content of a include*/
void getcontent(char* source){
	char* i = source;
	char* j = source;
	int flag = 0;
	while(*j != 0){
		*i = *j++;
		if((flag)&&(*i == '"'))
			break;
		if(flag)
			i++;
		if((!flag)&&(*i == '"'))
			flag = 1;
	}
	*i = 0;
}

/*check if a element is a the queue*/
int elementinqueue(const TSUQueue* uq, void* ele){
	int result = 0;
	int i;
	void* temp;
	for(i = 0; i < uq -> size(uq); ++i){
		uq -> remove(uq, &temp);
		if (strcmp(temp, ele)==0)
			result = 1;
		uq -> add(uq, temp);
	}
	return result;
}

/*build a temp queueu to avoid change in hashmap*/
const TSUQueue* tempqueuebuild(const TSUQueue* uq){
	int i;
	void* temp;
	const TSUQueue* result = TSUQueue_create();
	for(i = 0; i < uq -> size(uq); ++i){
		uq -> remove(uq, &temp);
		result -> add(result, strdup(temp));
		uq -> add(uq, temp);
	}
	return result;
}

void printdeps(const TSUQueue* printed, const TSUQueue* toprocess){
	char* processitem;
	void* temp;
	TSUQueue* itemlist;
	const TSUQueue* tempqueue;
	/*make sure everything in to process is dealed*/
	while(!(toprocess -> isEmpty(toprocess))){
		toprocess -> remove(toprocess, (void**)&processitem);
		hashmap -> get(hashmap, processitem, (void**)&itemlist);
		/*build a new queue base on old one to avoid the direct change of value in hashmap*/
		tempqueue = tempqueuebuild(itemlist);
		while(!(tempqueue -> isEmpty(tempqueue))){
			tempqueue -> remove(tempqueue, &temp);
			if (!elementinqueue(printed, temp)){
				printf(" %s", temp);
				printed -> add(printed, temp);
				toprocess -> add(toprocess, temp);
			}
		}
		tempqueue -> destroy(tempqueue, free);
	}
	toprocess -> destroy(toprocess, free);
	printed -> destroy(printed, free);
}

TSUQueue* process(char* afile, TSUQueue* deps){
	int fd = openfile(afile);
	char line[300];
	/*if can't open the file end the program*/
	if (fd == -1){
		printf("Could not open file %s\n", afile);
		exit(1);
		return NULL;
	}
	while(mygetline(fd, line, 300) > 0){
		removespaces(line);
		if (includecheck(line)){
			getcontent(line);
			char* temp = strdup(line);
			deps -> add(deps, temp);
			if (!(hashmap -> containsKey(hashmap, temp))){
				hashmap -> put(hashmap, temp, (void*)TSUQueue_create(), NULL);
				workqueue -> add(workqueue, temp);
			}
		}
	}
	close(fd);
	return deps;
}

/*helper function used by worker threads to get element from workqueue*/
char* removefromworkqueue(){
	pthread_mutex_lock(&mutex);
	activethreads--;
	char* result = NULL;
	while((workqueue -> isEmpty(workqueue))&&(activethreads > 0)){
		pthread_cond_wait(&cond, &mutex);
	}
	if (!(workqueue -> isEmpty(workqueue))){
		workqueue -> remove(workqueue, (void**)&result);
		activethreads++;
	}else{
		result = NULL;
	}
	pthread_cond_broadcast(&cond);
	pthread_mutex_unlock(&mutex);
	return result;
}

/*thread function for workers threads*/
void* threadfunction(void* i){
	TSUQueue* deps;
	char* keyname;
	while(!(workqueue -> isEmpty(workqueue))){
		keyname = removefromworkqueue();
		if (keyname == NULL)
			break;
		hashmap -> get(hashmap, keyname, (void**)&deps);
		deps = process(keyname, deps);
		hashmap -> put(hashmap, keyname, deps, NULL);
	}
}

int main(int argc, char *argv[]){
	int i;
	void* keyname;
	const TSUQueue* value;
	const TSUQueue* printed;
	const TSUQueue* toprocess;
	/*check the number of threads number if no build two worker threads*/
	if (getenv("CRAWLER_THREADS") != NULL)
		threadnumber = atoi(getenv("CRAWLER_THREADS"));
	pthread_t workers[threadnumber];
	workqueue = TSUQueue_create();
	hashmap = TSHashMap_create(0, 0.0);
	/*put the local address in path list first*/
	pathlist[0] = "./";
	plindex++;
	/*check -I extra address*/
	for (startplace; startplace < argc; startplace++){
		if (flagcheck(argv[startplace])){
			pathlineadd(argv[startplace]);
		}else{
			break;
		}
	}
	/*put cpath in path list*/
	if (getenv("PATH") != NULL)
		separatercpath(getenv("PATH"));
	/*build threads*/
	for (i = 0; i < threadnumber; ++i)
		pthread_create(&workers[i], NULL, threadfunction, NULL);
	for(i = startplace; i < argc; ++i){
		pthread_mutex_lock(&mutex);
		legalfailenamecheck(argv[i]);
		value = TSUQueue_create();
		value -> add(value, argv[i]);
		hashmap -> put(hashmap, ofile(argv[i]), (void*)value, NULL);
		workqueue -> add(workqueue, argv[i]);
		value = TSUQueue_create();
		hashmap -> put(hashmap, argv[i], (void*)value, NULL);
		/*tell every threads weak up and proces*/
		pthread_cond_broadcast(&cond);
		pthread_mutex_unlock(&mutex);
	}
	/*wait for all workers finish*/
	for (i = 0; i < threadnumber; ++i)
		pthread_join(workers[i], NULL);
	workqueue -> destroy(workqueue, free);
	for(i = startplace; i < argc; ++i){
		keyname = ofile(argv[i]);
		printf("%s:", keyname);
		printed = TSUQueue_create();
		printed -> add(printed, keyname);
		toprocess = TSUQueue_create();
		toprocess -> add(toprocess, keyname);
		printdeps(printed, toprocess);
		printf("\n");
	}
	/*destory these staff*/
	hashmap -> destroy(hashmap, NULL);
	pthread_mutex_destroy(&mutex);
	pthread_cond_destroy(&cond);
}