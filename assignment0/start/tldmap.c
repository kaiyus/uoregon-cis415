#include "tldmap.h"
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

struct tldnode {
	char* name;
	long value;
	TLDNode* left;
	TLDNode* right;
};

typedef struct tldmap_data {
	TLDNode* root;
	long cont;
} TLDMap_Date;


int strcicmp(char *a, char *b) {
    for (;; a++, b++) {
        int d = tolower(*a) - tolower(*b);
        if (d != 0 || !*a)
            return d;
    }
}

static int tld_insert(const TLDMap *tld, char *theTLD, long v){
	TLDMap_Date *mapdata = (TLDMap_Date *)(tld->self);
	// printf("start name check: %lu   %s\n", mapdata->cont, theTLD);
	TLDNode **update = &(mapdata->root);
	TLDNode *temp = *update;
	while(temp != NULL){
		int r = strcicmp(theTLD, temp -> name);
		// printf("search name cheack: %lu  %s   %s\n", mapdata->cont, temp-> name, theTLD);
		if (r < 0){
			update = &(temp -> left);
			temp = temp -> left;
		}else if (r > 0){
			update = &(temp -> right);
			temp = temp -> right;
		}else{
			return 0;
		}
	}
	TLDNode *new = (TLDNode *)malloc(sizeof(TLDNode));
	new -> name = strdup(theTLD);
	new -> value = v;
	new -> left = NULL;
	new -> right = NULL;
	// printf("%s  %lu\n", new -> name, new -> value);
	*update = new;
	mapdata -> cont += 1;
	// printf("root name: %s\n", mapdata->root->name);
	return 1;
}

static int tld_reassign(const TLDMap *tld, char *theTLD, long v){
	TLDMap_Date *mapdata = (TLDMap_Date *)(tld->self);
	TLDNode *temp = mapdata -> root;
	while(temp != NULL){
		int r = strcicmp(theTLD, temp -> name);
		// printf("reassign temp name: %s TLD name: %s\n", temp->name, theTLD);
		if (r < 0){
			temp = temp -> left;
		}else if(r > 0){
			temp = temp -> right;
		}else{
			temp -> value = v;
			return 1;
		}
	}
	return 0;
}

static int tld_lookup(const TLDMap *tld, char *theTLD, long *v){
	TLDMap_Date *mapdata = (TLDMap_Date *)(tld->self);
	TLDNode *temp = mapdata -> root;
	while(temp != NULL){
		int r = strcicmp(theTLD, temp -> name);
		if (r < 0){
			temp = temp -> left;
		}else if(r > 0){
			temp = temp -> right;
		}else{
			*v = temp -> value;
			return 1;
		}
	}
	return 0;
}

static void tldnodefree(TLDNode *node){
	if (node -> left != NULL){
		tldnodefree(node -> left);
	}
	if (node -> right != NULL){
		tldnodefree(node -> right);
	}
	free(node -> name);
	free(node);
}

static void tld_destroy(const TLDMap *tld){
	TLDMap_Date *mapdata = (TLDMap_Date *)(tld->self);
	TLDNode *root = mapdata -> root;
	if (root != NULL)
	{
		tldnodefree(root);
	}
}

static int inorder(TLDNode *root, void** tmp, long i){
	// printf("%ld\n", root->value);
	if (root->left != NULL){
		i = inorder(root->left, tmp, i);
	}
	tmp[i] = root;
	// printf("%lu   %s\n", i, root->name);
	i++;
	if (root->right != NULL){
		i = inorder(root->right, tmp, i);
	}
	return i;
}

static void** gettreeadress(const TLDMap *tld){
	TLDMap_Date *mapdata = (TLDMap_Date *)(tld->self);
	void **tmp = NULL;
	if (mapdata->cont > 0L){
		size_t nbytes = mapdata->cont * sizeof(void *);
		tmp = (void **)malloc(nbytes);
		long i = 0;
		inorder(mapdata->root, tmp, i);
	}
	return tmp;
}


static const Iterator * tld_itCreate(const TLDMap *tld){
	TLDMap_Date *mapdata = (TLDMap_Date *)(tld->self);
	const Iterator *it = NULL;
	void ** tmp = gettreeadress(tld);
	if (tmp != NULL){
		it = Iterator_create(mapdata->cont, tmp);
		if (it == NULL){
			free(tmp);
		}
	}
	return it;
}


static TLDMap template = {NULL, tld_destroy, tld_insert, tld_reassign, tld_lookup, tld_itCreate};

char *TLDNode_tldname(TLDNode *node){
	return node -> name;
}

long TLDNode_count(TLDNode *node){
	return node -> value;
}

const TLDMap *TLDMap_create(void){
	TLDMap *map = (TLDMap *)malloc(sizeof(TLDMap));

	if (map != NULL){
		TLDMap_Date *data = (TLDMap_Date *)malloc(sizeof(TLDMap_Date));
		if (data != NULL){
			data -> cont = 0;
			data -> root = NULL;
			*map = template;
			map->self = data;
		} else {
			free(map);
			map = NULL;
		}
	}
	return map;
}